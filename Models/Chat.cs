namespace RealTimeChatApplicationAPI.Models
{
    public class Chat
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Avatar { get; set; }
        public string Meta { get; set; }
        public string Status { get; set; }
    }
}