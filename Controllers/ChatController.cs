using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using RealTimeChatApplicationAPI.Repositories;
using RealTimeChatApplicationAPI.Models;

namespace RealTimeChatApplicationAPI.Controllers
{
    [Route("API/[controller]")]
    public class ChatController : Controller
    {
        private IChatRepository chatRepository;

        public ChatController(IChatRepository chatRepository)
        {
            this.chatRepository = chatRepository;
        }

        [HttpGet("GetAll")]
        public IEnumerable<Chat> GetAll()
        {
            return chatRepository.GetAll();
        }
    }
}