using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using RealTimeChatApplicationAPI.Repositories;
using RealTimeChatApplicationAPI.Models;

namespace RealTimeChatApplicationAPI.Controllers
{
    [Route("API/[controller]")]
    public class ConversationController : Controller
    {
        private IConversationRepository conversationRepository;

        public ConversationController(IConversationRepository conversationRepository)
        {
            this.conversationRepository = conversationRepository;
        }

        [HttpGet]
        public IEnumerable<Conversation> GetAll()
        {
            return conversationRepository.GetAll();
        }
    }
}