using System.Collections.Generic;
using RealTimeChatApplicationAPI.Models;

namespace RealTimeChatApplicationAPI.Repositories
{
    public interface IChatRepository
    {
        IEnumerable<Chat> GetAll();
    }
}