using System.Collections.Generic;
using RealTimeChatApplicationAPI.Models;

namespace RealTimeChatApplicationAPI.Repositories
{
    public class ChatRepository : IChatRepository
    {
        private List<Chat> chats;

        public ChatRepository()
        {
            chats = new List<Chat>();
            
            for(var i = 1; i < 10; i++) 
            {
                chats.Add(CreateChat(i));
            }
        }

        public IEnumerable<Chat> GetAll()
        {
            return chats.AsReadOnly();
        }
        
        private Chat CreateChat(int id) 
        {
            return new Chat 
            {
                Id = id,
                Title = "Chat " + id,
                Avatar = "assets/icons/user-icon-default.svg",
                Meta = "13:37",
                Status = "Lorem Ipsum Test"
            };
        }
    }
}