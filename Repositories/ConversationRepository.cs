using System.Collections.Generic;
using RealTimeChatApplicationAPI.Models;

namespace RealTimeChatApplicationAPI.Repositories
{
    public class ConversationRepository : IConversationRepository
    {
        private List<Conversation> conversations;

        public ConversationRepository()
        {
            conversations = new List<Conversation>();
        }

        public IEnumerable<Conversation> GetAll()
        {
            return conversations.AsReadOnly();
        }
    }
}