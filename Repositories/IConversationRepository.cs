using System.Collections.Generic;
using RealTimeChatApplicationAPI.Models;

namespace RealTimeChatApplicationAPI.Repositories
{
    public interface IConversationRepository
    {
        IEnumerable<Conversation> GetAll();
    }
}